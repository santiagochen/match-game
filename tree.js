

/*
 * Class of TreeView
*/

define(['jquery'], function($){

function TreeView() {
  this.initKeyNav();
  $(".tree-content-holder .tree-item").on('click', function(e) {
    var path;
    if (e.target.nodeName !== "A") {
      path = $('.tree-item-file-name a', this).attr('href');
      return Turbolinks.visit(path);
    }
  });
  $('span.log_loading:first').removeClass('hide');
}

TreeView.prototype.initKeyNav = function() {
  var li, liSelected;
  li = $("tr.tree-item");
  liSelected = null;
  return $('body').keydown(function(e) {
    var next, path;
    if ($("input:focus").length > 0 && (e.which === 38 || e.which === 40)) {
      return false;
    }
    if (e.which === 40) {
      if (liSelected) {
        next = liSelected.next();
        if (next.length > 0) {
          liSelected.removeClass("selected");
          liSelected = next.addClass("selected");
        }
      } else {
        liSelected = li.eq(0).addClass("selected");
      }
      return $(liSelected).focus();
    } else if (e.which === 38) {
      if (liSelected) {
        next = liSelected.prev();
        if (next.length > 0) {
          liSelected.removeClass("selected");
          liSelected = next.addClass("selected");
        }
      } else {
        liSelected = li.last().addClass("selected");
      }
      return $(liSelected).focus();
    } else if (e.which === 13) {
      path = $('.tree-item.selected .tree-item-file-name a').attr('href');
      if (path) {
        return Turbolinks.visit(path);
      }
    }
  });
};

return TreeView;


})
