
define(['jquery','btsp', 'time'], function($, undefined, time){

	var that = this;

	var _html = $('.mes');

	function equip(obj){

		_html.find('.modal-header>h4').text( obj.title );
		_html.find('.modal-body').text( obj.des );
		_html.find('.modal-footer>button').text( obj.btn );

		return _html;
	}

	



	return{

		show: function(obj){
			equip( obj ).modal();
		},

		close: function(){
			_html.modal('hide');
		},


	}


});