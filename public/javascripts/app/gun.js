
define(['jquery'],function($){

	function gun(powder){
		this.powder = [];
		this.bullets = [];
		this.loadpowder(powder);
	}

	gun.prototype.loadpowder = function(p){
		
		//here we double the bul for match
		var that = this;

		$.each(p, function(index, data){

			that.powder.push( data );
			that.powder.push( data );

		});

		this.shuffle();

	}

	gun.prototype.shuffle = function(){

		var that = this;

		var _length = this.powder.length;

		for(var n=0; n<_length; n++){
			
			var _index = parseInt(Math.random()*that.powder.length);
			
			that.bullets.push(that.powder[_index]);
			
			that.powder.splice(_index,1);
			
		}



	}

	gun.prototype.fire = function(num){

		return this.bullets.splice(0, num);

	}

	return gun;

})