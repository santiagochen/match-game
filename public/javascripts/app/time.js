define([ 'jquery','modal','mes'],function($, modal,mes){

	var that=this, _timesand, _interval, _speed=1000, _status, _control = $('span.timectl');

	function tick(){

		_interval = setInterval(function(){
				
			$('.yourtime').text( _timesand );

			if( _timesand>0 ){
				_timesand--;
			}else{
				
				clearInterval( _interval );
				_timesand = 0;
				_status = 'fail';
				modal.show( mes.fail )

			}
		}, _speed);

	}

	return {

		set : function(time){
			
			_timesand = time;
			
			_status = 'init';

			$('.yourtime').text( _timesand );
		},

		start : function(){
			
			tick();

			_status = 'on';

		},

		pause : function(){

			clearInterval( _interval );

			_status = 'pause';

			_control.removeClass('glyphicon-pause').addClass('glyphicon-play');

			modal.show(mes.pause);
		},

		match : function(){
			var that = this;

			clearInterval( _interval );

			_status = 'match';

			_control.removeClass('glyphicon-pause').addClass('glyphicon-play');

			modal.show(mes.match);
			
			setTimeout(function(){

				modal.close();
				that.resume();

			}, 800);
		},

		resume :function(){

			tick();

			_status = 'resume';

			_control.removeClass('glyphicon-play').addClass('glyphicon-pause');

			//modal.close();

		},

		fail : function(){

			clearInterval( _interval );
			_timesand = 0;
			_status = 'fail';

		},

		win : function(){

			clearInterval( _interval );
			_timesand = 0;
			_status = 'win';
			modal.show(mes.win);

		},

		getstatus : function(){

			return _status;

		},

		gettime : function(){

			return _timesand;

		}

		

	}


})