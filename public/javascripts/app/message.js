
define(['jquery'], function($){

	return {


		welcome: {

			title: "游戏介绍",

			des:'争取在时间结束之前翻对所有两辆配对的所有图片就能获得胜利',

			btn: '开始游戏',

			evt: 'start'

		},

		pause: {

			title: "游戏暂停",

			des:'您目前处于暂停状态',

			btn: '继续游戏',

			evt: 'resume'

		},

		match: {

			title: "匹配正确",

			des:'您完成一个匹配',

			btn: '继续游戏',

			evt: 'resume'

		},

		win: {

			title: "游戏胜利",

			des:'您胜利完成了游戏',

			btn: '重新游戏',

			evt: 'start'

		},

		fail: {

			title: "游戏失败",

			des:'真抱歉，您没能在规定时间完成游戏',

			btn: '重新游戏',

			evt: 'start'

		},


	}

});