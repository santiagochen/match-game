

requirejs( ['../config'],  function(config){

	require([
		'jquery',
		'data',
		'unit',
		'gun',
		'ob',
		'btsp',
		'modal',
		'mes',
		'time'
		],function($, data, Unit, Gun, ob, undefined, modal, mes, _time){

			function init(){

				var level = 1, time = 30;

				var _powder = data[level];

				ob.settle(_powder,time);

				var gun = new Gun( _powder.img );

				$('.game').empty();

				for( var m=0; m<_powder.unit; m++){
					
					//define how many times the unit would be created;
					//insert num into unit;
					var u = new Unit( m, gun.fire(1), 100/Math.sqrt(_powder.unit) );
					$('.game').append( u.view );

				}

				modal.show(mes.welcome);

			};
			init();


			//events
			$('span.timectl').on('click', onClickHandler);
			$('.mes').on('hidden.bs.modal', onHiddenHandler )//.on('shown.bs.modal',  onShownHandler );


			//handler
			function onHiddenHandler(e){

				switch( _time.getstatus() ){

					case 'init':
					_time.start();
					break;
					case 'pause':
					_time.resume();
					break;
					case 'fail':
					init();
					break;
					case 'win':
					init();
					break;

				}

			}

			/*function onShownHandler(e){

				switch( _time.getstatus() ){

					case 'init':
					modal.show(mes.welcome);
					break;
					case 'pause':
					modal.show(mes.pause);
					break;
					case 'match':
					modal.show(mes.match);
					setTimeout(modal.close, 1000);
					break;
					case 'fail':
					init();
					break;
					case 'win':
					init();
					break;

				}

			}*/

			function  onClickHandler(e){

				if( $(this).hasClass('glyphicon-pause')==true ){
					
					_time.pause();
					

				}else{
					
					_time.resume();
				}

			};


			


		});


} );