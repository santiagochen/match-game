define(['jquery','ob', 'notify'],function($,ob, notify){


	function Unit(index, mark, w){
		this.index = index;
		this.dev = arguments[3]?arguments[3]:false;
		this.mark = mark.join("");
		this.dimg = '../images/d.jpg';
		this.img = '../images/'+this.mark+'.jpg';
		this.width = w;
		this.isflip = false;
		this.view = $('<div class="unit"><p>'+this.mark+'</p><img src="'+this.dimg+'"></div>');
		this.init(mark);

	}

	Unit.prototype.init = function(mark){
		var that = this;
		this.stylish();
		this.view.on('click', this,  clickHandler );
		
	}

	Unit.prototype.stylish = function(){
		this.view.css({
			'width' : this.width+'%',
			'height' : 'auto'
		});		
	}

	Unit.prototype.flip = function(){
		var that = this;
		if( this.dev==true ) this.report();
				
		this.view.find('img').attr( 'src', (this.isflip?this.dimg:this.img) );;
		this.isflip = (this.isflip==false)?true:false;
	}

	Unit.prototype.disable = function(){
		this.view.off('click');
	}

	Unit.prototype.report = function(){
		console.log("report start: -------------------");
		console.log( 'mark: '+this.mark );
		console.log( 'isflip: '+this.isflip );
		console.log("report end: -------------------");
	}

	function clickHandler(e){
		
		e.preventDefault();
		
		e.data.flip();
		
		ob.compare(e.data);
		

	}

	return Unit;


})