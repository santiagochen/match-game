define(['jquery','time'],function($, _time){

var _match = [], _time, _pairs;

function arbitrage(){
	
	var _arr = _match;
	
	_match = [];

	if( _arr[0].mark==_arr[1].mark&&_arr[0].index!==_arr[1].index ){

		_pairs--;

		_arr[0].disable();
		_arr[1].disable();

		if( _pairs == 0 ){

			_time.win();

		}else{

			_time.match();

		}

		

	}else{
		
		_arr[0].view.addClass('animated shake');
		_arr[1].view.addClass('animated shake');

		setTimeout(function(){

			_arr[0].flip();
			_arr[1].flip();

			_arr[0].view.removeClass('animated shake');
			_arr[1].view.removeClass('animated shake');

		},1000)
	}

	
}


return {

	settle : function(data, time){
		_time.set(time);
		_pairs = data.unit/2;

	},

	compare : function(tar){
	
		_match.push(tar);

		if(_match.length<2){	
			return null;
		}else{
			return arbitrage();
		}	
	}



}

})