
//updated the config comments;
requirejs.config({
	//Prevent from Caching Required Scripts
	urlArgs: "cb=" + (new Date()).getTime(), 

	/* 华丽丽的分割线=============================================================================================================================================================*/
    baseUrl: '/javascripts',

    /* 华丽丽的分割线=============================================================================================================================================================*/
    uibaseUrl: '',

	paths : {

		/* 华丽丽的分割线=============================================================================================================================================================
		 * 配置所有第三方插件，抽离出来的公用功能模块 
		 * path directory: plugin
		 * 命名规范: plugin/{插件名称}/{插件版本号}/{插件文件名}
		 */
		
		//jquery这样的插件不用再介绍了吧！
		
		'jquery' : 'lib/jquery/dist/jquery.min',
		
		'btsp': 'lib/bootstrap/bootstrap.min',

		'table.level':'app/table.level',

		'unit':'app/unit',

		'data' : 'app/data',

		'time':'app/time',

		'gun' : 'app/gun',

		'ob' : 'app/ob',

		'notify' : 'app/notify',

		'mes' : 'app/message',

		'modal' : 'app/modal'
	},
	
	shim: {
        'btsp':{deps: ['jquery']},
        
    }
	
});

